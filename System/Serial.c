/*----------------------------------------------------------------------------
 * Name:    Serial.c
 * Purpose: Low level serial routines for STM32
 * Version: V1.00
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * Copyright (c) 2005-2007 Keil Software. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stm32f10x.h>                        // STM32F10x Library Definitions

#define USARTx   USART2                           // USART1 is used

/*----------------------------------------------------------------------------
  Write character to Serial Port
 *----------------------------------------------------------------------------*/
//int sendchar (int c) {
// 
//  if (c == '\n')  {
//    while (!(USARTx->SR & USART_FLAG_TXE));
//    USARTx->DR = 0x0D;
//  }	 
//	while (!(USARTx->SR & USART_FLAG_TXE));
//  	USARTx->DR = (c & 0x1FF);
// 
//// 	USART_SendData(USART2, c);
////    while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
//
//  return (c);
//}


/*----------------------------------------------------------------------------
  Read character from Serial Port   (blocking read)
 *----------------------------------------------------------------------------*/
/*int getkey (void) {

  while (!(USARTx->SR & USART_FLAG_RXNE));

  return ((int)(USARTx->DR & 0x1FF));
} */
