// Includes ////////////////////////////////////////////////////////////////////
#include "state_machine.h"

// globals //////////////////////////////////////////////////////////////////
const struct se_table_line se_table[SE_TABLE_LENGTH] =
{
	//@BRUNO: warum sind manche events bei action_3, und action_1 und action_2 sind auf A_NO_ACTION?

  //event					current_state	new_state		action_1					action_2			action_3				action_4			
  //-----					------------	---------		--------					--------			--------				--------			
//startup	
{	E_NO_EVENT,				S_INIT,			S_DISCHARGING,	{	A_LOW_POWER,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	},	},
{	E_ERROR,				S_INIT,			S_ERROR,		{	A_NO_ACTION,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},

{	E_NO_EVENT,				S_DISCHARGING,	S_DISCHARGING,	{	A_NO_ACTION,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_START_MPPT,			S_DISCHARGING,	S_MPPT,			{	A_START_CONVERSION,		A_INIT_MPPT,		A_NORMAL_POWER,			A_NO_ACTION	}, 	},
{	E_ERROR,				S_DISCHARGING,	S_ERROR,		{	A_NORMAL_POWER,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},

{	E_NO_EVENT,				S_MPPT,			S_MPPT,			{	A_NO_ACTION,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_STOP_MPPT,			S_MPPT,			S_CHARGING_CI,	{	A_START_CONVERSION,		A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_ERROR,				S_MPPT,			S_ERROR,		{	A_STOP_CONVERSION,		A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},

{	E_NO_EVENT,				S_CHARGING_CI,	S_CHARGING_CI,	{	A_NO_ACTION,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},	
{	E_START_MPPT,			S_CHARGING_CI,	S_MPPT,			{	A_INIT_MPPT,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_FULL,					S_CHARGING_CI,	S_CHARGING_CU,	{	A_SAVE_I_IN,			A_INIT_CU,			A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_NO_SUN,				S_CHARGING_CI,	S_DISCHARGING,	{	A_STOP_CONVERSION,		A_SAVE_U_IN,		A_LOW_POWER,			A_NO_ACTION	}, 	},
{	E_ERROR,				S_CHARGING_CI,	S_ERROR,		{	A_STOP_CONVERSION,		A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},

{	E_NO_EVENT,				S_CHARGING_CU,	S_CHARGING_CU,	{	A_NO_ACTION,			A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},	
{	E_FULL,					S_CHARGING_CU,	S_DISCHARGING,	{	A_STOP_CONVERSION,		A_LOW_POWER,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_NO_SUN,				S_CHARGING_CU,	S_DISCHARGING,	{	A_STOP_CONVERSION,		A_SAVE_U_IN,		A_LOW_POWER,			A_NO_ACTION	}, 	},
{	E_ERROR,				S_CHARGING_CU,	S_ERROR,		{	A_STOP_CONVERSION,		A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},

{	E_NO_EVENT,				S_ERROR,		S_ERROR,		{	A_ERROR,				A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_AKK_ERROR,			S_ERROR,		S_DISCHARGING,	{	A_CLEAR_ERROR,			A_LOW_POWER,		A_NO_ACTION,			A_NO_ACTION	}, 	},
{	E_ERROR,				S_ERROR,		S_ERROR,		{	A_ERROR,				A_NO_ACTION,		A_NO_ACTION,			A_NO_ACTION	}, 	},



//***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
};
