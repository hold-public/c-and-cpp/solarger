// Includes ////////////////////////////////////////////////////////////////////
#include "RCC.h"

#include "stm32f10x.h"


// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : RCC_Configuration
* Description    : Configures the different system clocks for all 
				   inputs and outputs.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void RCC_init(void)
{

/********** Enable the Analog Input Clock ***********************/ 
   /* ADCCLK = PCLK2/8 */
  RCC_ADCCLKConfig(RCC_PCLK2_Div8); 
    

/********** Enable DMA1 and DMA2 clocks *************************/ 
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

/********** Enable ADC1  clocks *********************************/	
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

///********** Enable USART2 clock *********************************/
//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
//
///********** Enable I2C2 clock ***********************************/
//RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, DISABLE);

/********** Enable the TIM2 Clock *******************************/
//   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); 

// /********** Enable the TIM3 Clock *******************************/
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

// /********** Enable the TIM4 Clock *******************************/
//   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

/********** Enable GPIOA, GPIOB, GPIOC and AFIO Clock  **********/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | 
  						 RCC_APB2Periph_GPIOB |
                         RCC_APB2Periph_GPIOC | 
						 RCC_APB2Periph_AFIO, ENABLE); 

/********** Enable SPI Clock  ***********************************/
//  RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2, ENABLE);



/********** Enable CAN Periph clock *****************************/
//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
}
