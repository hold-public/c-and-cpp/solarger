// Includes ////////////////////////////////////////////////////////////////////
#include "state_machine.h"

#include "actions.h"
#include "converter.h"
#include "GPIO.h"
#include "stm32f10x.h"

// privates ///////////////////////////////////////////////////////////////////
//The array g_event_store contains the numbers of the last 10 events occured
static e_event_type g_event_store[EVENT_STORE_SIZE] = {	E_NO_EVENT, E_NO_EVENT, E_NO_EVENT, E_NO_EVENT, E_NO_EVENT, 
													E_NO_EVENT, E_NO_EVENT, E_NO_EVENT, E_NO_EVENT, E_NO_EVENT}; //by default no events are stored


// globals ////////////////////////////////////////////////////////////////////
//The variable g_se_current_state contains the number of the current state of the state event machine
e_state_type g_se_current_state = S_INIT;	//inital state is S_INIT


// local function prototypes //////////////////////////////////////////////////
e_event_type load_occured_event(void);
unsigned int find_se_table_line(e_event_type se_occured_event);
void change_state(unsigned int se_line);
void execute_se_action(unsigned int se_table_line);
void clear_event_queue (unsigned int se_table_line);



// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	call_state_machine
* Description    : 	This subroutine executes actions that need to be done cyclicly. 
*					These actions are:		
*						- controll if some event happend
*						- change state if necessary
*						- update the display text
*						- generate actions
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli
* Date:          :  6.5.2011
*******************************************************************************/
void call_state_machine(void)
{
	 //The variable se_occured_event contains the number of the event that is currently handled by the state event machine	
	e_event_type se_occured_event = E_NO_EVENT;	//inital event is E_NO_EVENT
	unsigned int se_line = SE_NO_LINE_FOUND;	//this variable is used to store the number of a line in the state event table	
	
	se_occured_event = load_occured_event();						//load event occured	
	se_line = find_se_table_line(se_occured_event);				//find the line in the state event table where the current state and the occured event matches	
	if(se_line != SE_NO_LINE_FOUND)				//if a line was found in the state event table
	{											
		change_state(se_line);					//handle change of state
		execute_se_action(se_line);				//execute actions given in state event table
	} 											//end if
}


/*******************************************************************************
* Function Name  : 	load_occured_event
* Description    : 	This subroutine loads the event stored at the first position 
*					in the event queue (g_event_store) to the variable 
*					g_event_occured. The event loaded is deleted out of the 
*					event queue.
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S. Gatschet
* Date:          :  21.08.2006
*******************************************************************************/
e_event_type load_occured_event(void)
{
	unsigned int event_pointer = 0;
	e_event_type se_occured_event;
	
	se_occured_event = g_event_store[0];								//load oldest occured event
	while((g_event_store[event_pointer + 1] != E_NO_EVENT) && (event_pointer < EVENT_STORE_SIZE - 2))					//if event is stored at next position in event queue
	{
		g_event_store[event_pointer] = g_event_store[event_pointer+1];	//shift event one position forwards
		event_pointer++;												//increment event queue pointer
	}	
	g_event_store[event_pointer] = E_NO_EVENT; 							//delete event
	return se_occured_event;
}


/*******************************************************************************
* Function Name  : 	find_se_table_line
* Description    : 	the number of the line that fits to the current state and to the
*					event occured. If no line was found the value given in the constant
*					SE_NO_LINE_FOUND is returne
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S. Gatschet, P.Haldi
* Date:          :  08.08.2007
*******************************************************************************/
unsigned int find_se_table_line(e_event_type se_occured_event)
{
	unsigned int x = 0;
	unsigned int line = SE_NO_LINE_FOUND;

	while(x < (SE_TABLE_LENGTH/*+1*/))			//search occured event in state event table
	{					
		if((se_table[x].event == se_occured_event) && (se_table[x].current_state == g_se_current_state))	
		{	//if the event and the current state in the state event table matches with the current state and the occured event
			line = x;	//store the number of the line in the state event table
		} //end if
		x++;	//increment loop counter
	} //end while
	return line;
}
/*******************************************************************************
* Function Name  : 	change_state
* Description    : 	This subroutine does the following actions if the state of 
*					the state event machine will change: It clears the event 
*					queue, change from current state	to new state.
* Input          : 	se_line => the number of the line in the state event table
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli
* Date:          :  6.5.2011
*******************************************************************************/
void change_state(unsigned int se_line)
{
	if(g_se_current_state != se_table[se_line].new_state) //if current state is not new state
	{
		clear_event_queue(se_line);	//clear all events left in event queue if change of state
		g_se_current_state = se_table[se_line].new_state;	//change state	 	
	}
}


/*******************************************************************************
* Function Name  : 	execute_se_action
* Description    : 	This subroutine executes a action given in the state 
*					event table. The number of the action to be executed 
*					is handed over in the input action
* Input          : 	action => the number of the action to execute
* Output         : 	None
* Return         : 	None
* Author:        :  S. Gatschet
* Date:          :  208.08.2007
*******************************************************************************/
void execute_se_action(unsigned int se_table_line)
{
	unsigned char x;
	
	//unsigned char message[3];

	for(x=0; x<=3; x++)
	{
		//The following code sequence can be used to send the number of the action executet over the uart
		/*char_to_string (&message[0], se_table[se_table_line].action[x]);	//send number of action to execute
		message[2] = SE_LOG_NL;			//new line
		uart1_send_data (message, 3);	//send number of action over uart*/
		
		switch(se_table[se_table_line].action[x])		//if number of action to execute is 
		{
			case A_NO_ACTION:			; 						break;	//execute no action
 			case A_START_CONVERSION:	start_conversion();		break;
			case A_STOP_CONVERSION: 	stop_conversion();		break;
			case A_SAVE_U_IN:			save_U_IN();			break;
			case A_SAVE_I_IN:			save_I_IN();			break;
			case A_LOW_POWER:			aa_enter_low_power_mode(); break;
			case A_NORMAL_POWER:		aa_enter_normal_power_mode(); break;
			case A_INIT_CU:				init_CU();				break;
			case A_INIT_MPPT:			aa_mppt_init();			break;
 			case A_ERROR:				aa_error();				break;
 			case A_CLEAR_ERROR:			aa_clear_error();		break;
			default:					g_error = ACTION_ERROR; break;
				
									
		} //end switch
	}
}


/*******************************************************************************
* Function Name  : 	clear_event_queue
* Description    : 	This subroutine clears the event queue. All the events stored
*					in the array g_event_store are deleted if at the current 
*					transition a change of state will happen
* Input          : 	se_table_line =>	the number of the line in the state event table. The
*						acutal state and the new state given on these line will be compared 
* Output         : 	None
* Return         : 	None
* Author:        :  S. Gatschet
* Date:          :  22.08.06
*******************************************************************************/
void clear_event_queue(unsigned int se_table_line)
{
	unsigned char x;
	
	if(se_table[se_table_line].current_state != se_table[se_table_line].new_state)	//if current transition does a change of state
	{
		for(x=0; x<=9; x++)	//delete all events stored in the event queue
		{
			g_event_store[x] = E_NO_EVENT; //delete event
		}
	}
}


/*******************************************************************************
* Function Name  : 	create_event
* Description    : 	This subroutine creates the event with the number given 
*					in the input event_number. Basicly it writes the number 
*					to the event occured queue. Therefore it first looks 
*					for the first empty space in the event occured queue and 
*					the writes the number
* Input          : 	event_number = the number of the event that occured
* Output         : 	None
* Return         : 	None
* Author:        :  S. Gatschet
* Date:          :  21.08.2007
*******************************************************************************/
void create_event(e_event_type event_number)
{
	int event_pointer = 0;	
	
	while((g_event_store[event_pointer] != E_NO_EVENT) && (event_pointer < EVENT_STORE_SIZE - 1))	//if space in event queue is not empty
	{
		event_pointer++;												//increment event queue pointer
	}
	g_event_store[event_pointer] = event_number; 						//store event number
}
