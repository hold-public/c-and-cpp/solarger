// Includes ////////////////////////////////////////////////////////////////////
#include "SPI.h"

#include "CPLD.h"
#include "GPIO.h"
#include "STM32F10x_SPI.h"


// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	SPI_init
* Description    : 	initializes the SPI-Module
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli
* Date:          :  20.8.2012
*******************************************************************************/
void SPI_init(void)
{
	/* Initialize the SPI2 according to the SPI_InitStructure members */
	SPI_InitTypeDef SPI_InitStructure;
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;	 //Cloock Idle high
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; //Data is captured on the second edge
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;	 //Internal SS-Signal controlled by SSI-Bit
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;	//SPI_CLK-frequency = 72MHz/(2*Prescaler)
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI2, &SPI_InitStructure);

}


/*******************************************************************************
* Function Name  : 	send_SPI_messages
* Description    : 	sends all the neccessary information to the CPLD
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli, P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void send_SPI_messages(void) {
	send_2Byte_SPI((1 << 13) + g_integer_N);					// Send the SPI data integer_N
	send_2Byte_SPI((2 << 13) + g_switching_value);   			// Send the SPI data Preload
	send_2Byte_SPI((3 << 13) + g_fractional_N);					// Send the SPI data fractional_N
	send_2Byte_SPI((4 << 13) + g_dead_value);   				// Send the SPI data dead
	send_2Byte_SPI((7 << 13) + g_power_holdback_value);			// Send the SPI data Osc_value
}


/*******************************************************************************
* Function Name  : 	send_2Byte_SPI
* Description    : 	sends two Bytes via SPI 
* Input          : 	data with 2 Bytes (unsigned short)
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli
* Date:          :  8.6.2013
*******************************************************************************/
void send_2Byte_SPI(unsigned short data)
{
	
	// calculate the parity and give it to the SPIOK Pin
	if (parity(data) == 0) {
		GPIO_ResetBits(GPIOB, SPIOK);
	} else {
		GPIO_SetBits(GPIOB, SPIOK);
	}
	
	/*Reset the NSS*/
	GPIO_ResetBits(GPIOB, NSS);	 
	/*Enable SPI2*/  	
	SPI_Cmd(SPI2, ENABLE);
	/* Wait for SPI2 Tx buffer empty */
	while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
	/* Send SPI2 data */
	SPI_I2S_SendData(SPI2, data);
	/* Wait for SPI2 end of transmission */
	while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET);

	/* Set the NSS */
	GPIO_SetBits(GPIOB, NSS);
	/* Disable SPI2 */	   	
	SPI_Cmd(SPI2,DISABLE);
}


/*******************************************************************************
* Function Name  : parity
* Description    : calculates the parity
* Input          : int val
* Output         : 
* Return         : parity
* Date           : 8. Juni 2012
* Author         : B. Binggeli
* 
*******************************************************************************/
int parity(int val)
{
	val ^= val >> 8;
  	val ^= val >> 4;
  	val ^= val >> 2;
  	val ^= val >> 1;
  	return val & 1;
}

