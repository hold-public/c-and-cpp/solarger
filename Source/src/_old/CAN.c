// Includes ////////////////////////////////////////////////////////////////////
#include "CAN.h"

#include "ADC.h"
#include "CPLD.h"
#include "GPIO.h"
#include "state_machine.h"
#include "stm32f10x_can.h"

// globals ////////////////////////////////////////////////////////////////////
char	g_CAN_address=0;


// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	CAN_debug
* Description    : 	sends a number with the ID 123 for debugging purpose
* Input          : 	data to send
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void CAN_debug(unsigned int data) {
	CanTxMsg TxMessage;	

	TxMessage.StdId = 0x123;
  	TxMessage.ExtId = 0x01;
  	TxMessage.RTR = CAN_RTR_DATA;	 		// Specifies the type of frame for the received message.
  	TxMessage.IDE = CAN_ID_STD;		 		// Specifies the type of identifier for the message that will be received.
											// This parameter can be a value of @ref CAN_identifier_type */
  	TxMessage.DLC = 2;		         		// Data Length Code
	TxMessage.Data[0] = data 		& 0xFF;
	TxMessage.Data[1] = (data >> 8) & 0xFF;

	CAN_Transmit(CAN1, &TxMessage);
}


/*******************************************************************************
* Function Name  : 	CAN_init
* Description    : 	initializes the CAN-Module
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli
* Date:          :  8.6.2012
*******************************************************************************/
void CAN_init(void)
{

	CAN_InitTypeDef        CAN_InitStructure;
	CAN_FilterInitTypeDef  CAN_FilterInitStructure;

//	/* Old init. of CAN*/

	// CAN register init 
	CAN_DeInit(CAN1);
	CAN_StructInit(&CAN_InitStructure);
	
	// CAN cell init 
	CAN_InitStructure.CAN_TTCM=DISABLE;
	CAN_InitStructure.CAN_ABOM=DISABLE;
	CAN_InitStructure.CAN_AWUM=DISABLE;
	CAN_InitStructure.CAN_NART=ENABLE;
//	CAN_InitStructure.CAN_ABOM=ENABLE;
//	CAN_InitStructure.CAN_AWUM=ENABLE;
//	CAN_InitStructure.CAN_NART=DISABLE; 
	                              
	CAN_InitStructure.CAN_RFLM=ENABLE;                               
	CAN_InitStructure.CAN_TXFP=DISABLE;
	CAN_InitStructure.CAN_Mode=CAN_Mode_Normal;                      // CAN hardware operates in normal mode
	CAN_InitStructure.CAN_SJW=CAN_SJW_1tq;                           // 1 time quanta is allowed to lengthen or shorten a bit
	CAN_InitStructure.CAN_BS1=CAN_BS1_6tq;                           // 3 time quanta in Bit Segment 1.
	CAN_InitStructure.CAN_BS2=CAN_BS2_5tq;                           // 5 time quanta in Bit Segment 2.
	CAN_InitStructure.CAN_Prescaler=6;                              // APB1 Clock = 36Mhz /24 = 1.5 MHz /12 time quantum =125 kHz
	CAN_Init(CAN1,&CAN_InitStructure);                               // with PLL 3 ->1Mb/s ; 6 ->500kb/s
	
	// CAN filter init
	CAN_FilterInitStructure.CAN_FilterNumber=0;
  	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;
  	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
  	CAN_FilterInitStructure.CAN_FilterIdHigh=0x0000;
  	CAN_FilterInitStructure.CAN_FilterIdLow=0x0000;
  	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=0x0000;
  	CAN_FilterInitStructure.CAN_FilterMaskIdLow=0x0000;
  	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=CAN_FIFO0;
  	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;
  	CAN_FilterInit(&CAN_FilterInitStructure);


	CAN_ITConfig(CAN1,CAN_IT_FMP0, ENABLE);
	CAN_ITConfig(CAN1,CAN_IT_FOV0, ENABLE);

}


/*******************************************************************************
* Function Name  : 	CAN_receive_Msg
* Description    : 	checks if a CAN-Message arrived
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli, P.Haldi
* Date:          :  8.6.2012
*******************************************************************************/
void CAN_receive_Msg(void)
{
	CanRxMsg RxMessage;
	int Par_ID;			//Parameter ID
	int gen_ID;	 		//Generator ID
	int	dir;		//Direction 1 for BG->Geni 0 for Geni->BG

	
	while((CAN_MessagePending(CAN1,CAN_FIFO0)) > 0)            // CAN Message in Buffer?                               
    {
		CAN_Receive(CAN1,CAN_FIFO0, &RxMessage);
		dir				=(RxMessage.StdId & 0x400)>>10; 
		gen_ID	  		=(RxMessage.StdId & 0x3E0)>>5;
		Par_ID			= RxMessage.StdId & 0x01F;
		

		if((gen_ID==g_CAN_address)&&(dir==1))
		{
			switch (Par_ID) 
			{
			//Settings for operation
			case 0x01 : g_Pout_soll       	= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;
			case 0x02 : g_fout_soll       	= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]);
						g_f_start		  	= g_fout_soll; break;
			case 0x03 : //g_Osc_Time0			=(RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]);
// 						g_Osc_Time1			=g_Osc_Time0;
// 						g_Osc_value_start	=(((double)1e-9*g_Osc_Time0*g_ringosc_freq+g_dead_value)+0.5);
// 						g_Osc_value0		=(g_Osc_value_start);
// 						g_Osc_value1 		=(g_Osc_value_start);
			
						// Befehl wird nicht unterstützt!!
						reset_CPLD();
						create_event(E_ERROR);
						g_error=NOT_AUS_ERROR;
						break;
			case 0x04 : //g_Usupply_soll		=(RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;
						// Befehl wird nicht unterstützt!!
						reset_CPLD();
						create_event(E_ERROR);
						g_error=NOT_AUS_ERROR;
						break;
			//sweep settings
			case 0x05 :	g_f_sweep_start		= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); calc_sweep(); break;
			case 0x06 :	g_f_sweep_stop		= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); calc_sweep(); break;
			case 0x07 :	g_f_step			= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;
			case 0x08 : g_t_sweep			= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;
			//wobble settings
			case 0x09 : g_wobble_max		= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;
			case 0x0A : g_wobble_period		= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0])/g_t_wobble; break;
						
			//operation mode settings
			case 0x10 :	//g_div_ring			=(RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;
						// Befehl wird nicht unterstützt!!
						reset_CPLD();
						create_event(E_ERROR);
						g_error = NOT_AUS_ERROR;
			case 0x11 :	g_selected_mode		= (e_selected_mode)(RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); break;	   //
			case 0x12 :	g_sweep_mode		= (RxMessage.Data[2]*65536 + RxMessage.Data[1]*256 + RxMessage.Data[0]); if(g_sweep_mode == 1){g_fout_soll=g_f_sweep_start;} break;
			
			case 0x13 : g_degas = !g_degas; break;

			case 0x14 : g_power_percentage	= (RxMessage.Data[0]); break;
			case 0x15 : g_switching_time = (RxMessage.Data[1] * 256 + RxMessage.Data[0]); break;
			case 0x16 : g_dead_time = (RxMessage.Data[1] * 256 + RxMessage.Data[0]); break;
			case 0x17 : g_overlap_time = (RxMessage.Data[1] * 256 + RxMessage.Data[0]); break;
			//TODO: choose peak
			//create event
			case 0x1F : switch(RxMessage.Data[0])
						{
							case 0x01 : create_event(E_INIT); break;
							case 0x02 : create_event(E_START); break;
							case 0x04 : create_event(E_STOP); break;
							case 0x08 : reset_CPLD();
										create_event(E_ERROR);
										g_error = NOT_AUS_ERROR; break;// TODO: noch nicht unterstützt! create_event(E_START_SWEEP); break;
							case 0x10 : create_event(E_ERROR_REPAIRED); break;
							case 0x20 : 
								reset_CPLD();
								create_event(E_ERROR);
								g_error = NOT_AUS_ERROR; break;
							case 0x40 : create_event(E_SEND_PARAMETER); break;
							default	  : create_event(E_ERROR);
										g_error = CAN_COM_ERROR; break;
						}
			default   : break;
			}
		}
		else if((gen_ID==g_CAN_address)&&(dir==0))
		{
			g_error = CAN_ADDRESS_ERROR;
		}    	  
	}

}


/*******************************************************************************
* Function Name  : 	CAN_send_Msg
* Description    : 	Sends a Message over the CAN-Bus
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli
* Date:          :  8.6.2012
*******************************************************************************/
void CAN_send_Msg(int id, int data) {

CanTxMsg TxMessage;
	
	TxMessage.StdId = id;
  	TxMessage.ExtId = 0x01;
  	TxMessage.RTR = CAN_RTR_DATA;	 // Specifies the type of frame for the received message.
  	TxMessage.IDE = CAN_ID_STD;		 // Specifies the type of identifier for the message that will be received.
                                     //   This parameter can be a value of @ref CAN_identifier_type */
  	TxMessage.DLC = 1;		         // Data Length Code
	TxMessage.Data[0] = data;

	CAN_Transmit(CAN1, &TxMessage);
}



/*******************************************************************************
* Function Name  : 	monitoring
* Description    : 	this function sends the measured data
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli
* Date:          :  8.11.2012
*******************************************************************************/
void monitoring(void)
{
	int select[] = {1,1,0,0,0,0,0,0,0,0,0,0,0};
	static int counter = 0;
	if(select[counter] == 1)
	{
		CAN_send_number(65 + counter, (int)(g_ADC_value_filtered[counter]));	//ADC_I_IN und ADC_U_DC_PLUS
	}  
	
	if(counter == 4)
	{		
		CAN_send_Msg(0, 0);
	}
	
	if(counter == 5)
	{
		// Nicht unterstützt:
		CAN_send_number(24, 0); // CAN_send_number(24, ((double)(g_Osc_value0-g_dead_value)*1e9)/(double)g_ringosc_freq);
	}
	
	if(counter == 6)
	{
		CAN_send_number(103, (int)(g_fout_soll));
	}
	
	if(counter >= 10)
	{
		CAN_send_number(71, (int)(g_ADC_value_filtered[ADC_T_KK]));
		counter=0;
	}
	
	counter++;
}


/*******************************************************************************
* Function Name  : 	CAN_send_number
* Description    : 	sends a Number over the CAN-Bus
* Input          : 	ID and Nr
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli
* Date:          :  8.6.2012
*******************************************************************************/
void CAN_send_number(int id, int Nr) 
{
	CanTxMsg TxMessage;
	

	if (Nr<0)
	{
		TxMessage.Data[0] = 45;
		Nr=Nr*(-1);
	}
	else
	{
		TxMessage.Data[0] = 43;

	}
	if((Nr>=999999))
	{
		TxMessage.Data[1] = 57;
		TxMessage.Data[2] = 57;
		TxMessage.Data[3] = 57;
		TxMessage.Data[4] = 57;
		TxMessage.Data[5] = 57;
		TxMessage.Data[6] = 57;
		TxMessage.Data[7] = 57;
	}
	else
	{
		TxMessage.Data[1]=Nr/100000;
		Nr=Nr-TxMessage.Data[1]*100000;
		TxMessage.Data[1]=TxMessage.Data[1]+48;
		TxMessage.Data[2]=Nr/10000;
		Nr=Nr-TxMessage.Data[2]*10000;
		TxMessage.Data[2]=TxMessage.Data[2]+48;
		TxMessage.Data[3]=Nr/1000;
		Nr=Nr-TxMessage.Data[3]*1000;  
		TxMessage.Data[3]=TxMessage.Data[3]+48;
		TxMessage.Data[4]=Nr/100;
		Nr=Nr-TxMessage.Data[4]*100;
		TxMessage.Data[4]=TxMessage.Data[4]+48;
		TxMessage.Data[5]=Nr/10;
		Nr=Nr-TxMessage.Data[5]*10;
		TxMessage.Data[5]=TxMessage.Data[5]+48;
		TxMessage.Data[6]=Nr/1;
		Nr=Nr-TxMessage.Data[5]*1;
		TxMessage.Data[6]=TxMessage.Data[6]+48;
	}

	TxMessage.StdId = id;
  	TxMessage.ExtId = 0x01;
  	TxMessage.RTR = CAN_RTR_DATA;	 // Specifies the type of frame for the received message.
  	TxMessage.IDE = CAN_ID_STD;		 // Specifies the type of identifier for the message that will be received.
                                     //   This parameter can be a value of @ref CAN_identifier_type */
  	TxMessage.DLC = 7;		         // Data Length Code

	CAN_Transmit(CAN1, &TxMessage);
}
