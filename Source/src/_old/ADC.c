// Includes ////////////////////////////////////////////////////////////////////
#include "ADC.h"

#include "converter.h"
#include "GPIO.h"
#include "state_machine.h"
#include "stm32f10x.h"


// privates ///////////////////////////////////////////////////////////////////
static ADC_InitTypeDef ADC_InitStructure;
static DMA_InitTypeDef DMA_InitStructure;
static const int 		Nr_of_Filtervalues[NR_OF_CHANNELS] = {4, 4, 4}; 								// Define here each number of Filtervalues!
static volatile short 	ADC_number_raw[NR_OF_CHANNELS]; 														// the convertet value of the analog input
static double		 	ADC_filter_value_sr[NR_OF_CHANNELS][MAX_SIZE_OF_FILTER_RING];
static int 				ADC_number_average_cache[NR_OF_CHANNELS][SIZE_OF_AVERAGE_CACHE];
static int				nr_of_acquisitions = 0;
static e_error determine_error_for_channel(e_ADC_channel channel);

// globals ////////////////////////////////////////////////////////////////////
volatile double g_ADC_value_filtered[NR_OF_CHANNELS];				// filtered values
volatile double g_ADC_value_measured[NR_OF_CHANNELS];									// filtered value of Iin Usupply Usupply/2Uum2 after peak Detektion


// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	ADC_init
* Description    : 	Configures the different Direct memory access.
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
void ADC_init(void)
{
	
	/* DMA1 channel1 configuration DMA= Data Memory Access----------------------------------------------*/
	DMA_DeInit(DMA1_Channel1);
	DMA_InitStructure.DMA_PeripheralBaseAddr 		= ADC1_DR_Address;		///< Address of ADC1 Data register (DR)
	DMA_InitStructure.DMA_MemoryBaseAddr 			= (uint32_t)&ADC_number_raw;
	DMA_InitStructure.DMA_DIR 						= DMA_DIR_PeripheralSRC; //DMA_DIR specifies if the peripheral is the source or destination
	DMA_InitStructure.DMA_BufferSize 				= 8;  //is equivalent to the size in globals.h !
	DMA_InitStructure.DMA_PeripheralInc 			= DMA_PeripheralInc_Disable;  //DMA_Peripherallinc specifies whether the Peripheral address register is incremented or not
	DMA_InitStructure.DMA_MemoryInc 				= DMA_MemoryInc_Enable;	  //DMA_MemoryInc specifies whether the memory address register is incremented or not
	DMA_InitStructure.DMA_PeripheralDataSize 		= DMA_PeripheralDataSize_HalfWord;	//DMA_PeripheralDataSize configures the Peripheral data width. HalfWord = 16 Bit
	DMA_InitStructure.DMA_MemoryDataSize 			= DMA_MemoryDataSize_HalfWord;	//DMA_MemoryDataSize defines the Memory data width. HalfWord = 16 Bit
	DMA_InitStructure.DMA_Mode 						= DMA_Mode_Circular; //DMA_Mode configures the operation mode of the DMAy Channelx. The circular buffer mode cannot be used if the memory-to-memory data transfer is configured on the selected Channel
	DMA_InitStructure.DMA_Priority 					= DMA_Priority_High; //DMA_Priority configures the software priority for the DMAy Channelx.
	DMA_InitStructure.DMA_M2M 						= DMA_M2M_Disable; //DMA_M2M enables the DMAy Channelx memory- to-memory transfer.
	DMA_Init(DMA1_Channel1, &DMA_InitStructure);
	  
	/* Enable DMA1 channel1 */
	DMA_Cmd(DMA1_Channel1, ENABLE);
	
	/* ADC1 configuration ------------------------------------------------------*/
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;	// ADC1 and ADC 2 operate in independent mode
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;			// ADC_ScanConvMode specifies whether the conversion is performed in Scan (multichannels) or Single (one channel) mode.                    
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;	// The conversion is performed in continous mode
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // the conversion is triggered by software
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;  //  Right data alingnment
	ADC_InitStructure.ADC_NbrOfChannel = 8;				 // 6 ADC-Channels 
	ADC_Init(ADC1, &ADC_InitStructure);
	
	
	/* ADC1 regular channel14 configuration. The rank in the regular group sequencer. Important for
	the order in the memory! The conversion time is:
	
	conversion Time = (cicles + 12.5 cicles)*ADC_CLK-Time
	
	Here it is:
	Prescaler ADCCLK = CLK/8 = 72 MHz /8 = 9MHz (See RCC.c) => The ADC_CLK-Time is 1/9MHz = 111.111 ns
	So with 55.5 cicles the total cicles are: 55.5+12.5 cicles = 68 cicles that mean a 
	
	conversion time = 68*111.111 ns = 7.555 us
	  
	Because we habe 5 channels with the scan_mode, every 37.77 us a channel is sampled
	This is a sample frequency of 26 kHz*/ 
	//             			 conv	channel				rank	samples  55.5 cicles
	ADC_RegularChannelConfig(ADC1, 	ADC_Channel_0, 		1, 		ADC_SampleTime_55Cycles5); // U_in
	ADC_RegularChannelConfig(ADC1, 	ADC_Channel_1, 		2, 		ADC_SampleTime_55Cycles5); // I_in
	ADC_RegularChannelConfig(ADC1, 	ADC_Channel_2, 		3, 		ADC_SampleTime_55Cycles5); // U_Bat

 
	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);
	
	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);
	
	/* Enable ADC1 reset calibaration register */   
	ADC_ResetCalibration(ADC1);
	
	/* Check the end of ADC1 reset calibration register */
	while(ADC_GetResetCalibrationStatus(ADC1));
	
	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1);
	/* Check the end of ADC1 calibration */
	while(ADC_GetCalibrationStatus(ADC1));
	     
	/* Start ADC1 Software Conversion */ 
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}




/*******************************************************************************
* Function Name  : 	convert_ADC_value
* Description    : 	converts all the fast channels, detects the peak and filters them 
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, s.Weyeneth, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
void convert_ADC_value(void) {	
	int ADC_number_fast[NR_OF_CHANNELS];
	static int ADC_number_sr[NR_OF_CHANNELS][SIZE_OF_PREFILTER];
	int ADC_number_average[NR_OF_CHANNELS];
	static int average_counter = 0;
	int filtervalue_nr, channel_nr;
	int sum;
	
	nr_of_acquisitions++;
//	convert ADC values to physical values
	ADC_number_fast[ADC_U_IN] 			= ADC_number_raw[ADC_U_IN];							
	ADC_number_fast[ADC_I_IN] 			= ADC_number_raw[ADC_I_IN];		
	ADC_number_fast[ADC_U_BAT] 			= ADC_number_raw[ADC_U_BAT];			
//	Inputfilter. Fill the buffer g_sensor_filter_values and calculate the mean
	for (channel_nr = 0; channel_nr < NR_OF_CHANNELS; channel_nr++) {					//repeat for every channel 
		for(filtervalue_nr = 0; filtervalue_nr < SIZE_OF_PREFILTER - 1; filtervalue_nr++)	{			//replace the old values in the filter-array shift-left
			ADC_number_sr[channel_nr][filtervalue_nr] = ADC_number_sr[channel_nr][filtervalue_nr + 1];  //shift left
		}
		ADC_number_sr[channel_nr][SIZE_OF_PREFILTER - 1] = ADC_number_fast[channel_nr]; //write the new to the last position
// calculate the mean of every buffer of the channel
		sum = 0;
		for(filtervalue_nr = 0; filtervalue_nr < SIZE_OF_PREFILTER; filtervalue_nr++) 
		{
			sum += ADC_number_sr[channel_nr][filtervalue_nr];
		}
		ADC_number_average[channel_nr] = sum / SIZE_OF_PREFILTER;	//division with 4, then write the filtered value to g_sensor_fast
//store values for peak-value detection
		ADC_number_average_cache[channel_nr][average_counter] = ADC_number_average[channel_nr];
	}
	average_counter++;
	if(average_counter == SIZE_OF_AVERAGE_CACHE)
	{	
		g_ADC_value_filtered[ADC_U_IN]	 	= find_peak_value(ADC_U_IN) 		* ADC_GAIN_U_IN;								
		g_ADC_value_filtered[ADC_I_IN]	 	= find_peak_value(ADC_I_IN) 		* ADC_GAIN_I_IN;		
		g_ADC_value_filtered[ADC_U_BAT]	 	= find_peak_value(ADC_U_BAT) 		* ADC_GAIN_U_BAT;				
		average_counter=0; 
//	Inputfilter. Fill the buffer g_sensor_filter_values and calculate the mean
		for(channel_nr = 0;channel_nr < NR_OF_CHANNELS; channel_nr++)					//repeat for each channel
		{
			for (filtervalue_nr=0;filtervalue_nr < (Nr_of_Filtervalues[channel_nr] - 1); filtervalue_nr++) {				//replace the old values in the filter-array shift-left
				ADC_filter_value_sr[channel_nr][filtervalue_nr] = ADC_filter_value_sr[channel_nr][filtervalue_nr + 1];  //shift left
			}
	
			ADC_filter_value_sr[channel_nr][Nr_of_Filtervalues[channel_nr] - 1] = g_ADC_value_filtered[channel_nr]; //write the new to the last position
											
			// calculate the mean of every buffer of the channel
			sum = 0;
			for(filtervalue_nr = 0; filtervalue_nr < Nr_of_Filtervalues[channel_nr]; filtervalue_nr++) {
				sum += ADC_filter_value_sr[channel_nr][filtervalue_nr];
			}
			g_ADC_value_measured[channel_nr] = (double) sum/Nr_of_Filtervalues[channel_nr];	// write the filtered value to g_sensor	
		}
	}

} 


/*******************************************************************************
* Function Name  : 	find_peak_value
* Description    : 	searches the largest nummer of the array ADC_number_average_cache
* Input          : 	For which channel the search occurs
* Output         : 	the largest value
* Return         : 	None
* Author:        :  B.Binggeli, s.Weyeneth, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
double find_peak_value(int channel_nr)
{
    int i;
	double peak = ADC_number_average_cache[channel_nr][0];       // start with max = first element

	for(i = 1; i < SIZE_OF_AVERAGE_CACHE; i++)
    {
    	if(ADC_number_average_cache[channel_nr][i] > peak)
		{
        	peak = (double)ADC_number_average_cache[channel_nr][i];
		}
	}
    return peak;                // return highest value in array
}


/*******************************************************************************
* Function Name  : 	ADC_Error_handler
* Description    : 	detects if any channel exeeds its limits. The check is 
*					only executed after a minimal Number of acquisitions
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, s.Weyeneth, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
void ADC_Error_handler(void)
{
	volatile double temp;
	const double c_sensor_limits[NR_OF_CHANNELS][2] =	   {{10000,		25000},				// U_in: 10..25V
															{0,			16000},				// I_in: 0..1.6A
															{10000,		15000}};  			// U_bat: 10..15V
	e_ADC_channel channel_nr;
	

	for(channel_nr = ADC_U_IN; channel_nr <= ADC_U_BAT; channel_nr++)					//repeat for each channel
	{	
		if (nr_of_acquisitions >= MIN_NR_OF_ACQUISITIONS) {
			temp = g_ADC_value_filtered[channel_nr];
			if(g_ADC_value_filtered[channel_nr] > c_sensor_limits[channel_nr][LIMIT_MAX])
			{		
				//TODO: actions?
				g_error = determine_error_for_channel(channel_nr);		
				create_event(E_ERROR);	
			}
			else if(g_ADC_value_filtered[channel_nr] < c_sensor_limits[channel_nr][LIMIT_MIN])
			{		
				//TODO: actions?
				g_error = determine_error_for_channel(channel_nr);
				create_event(E_ERROR);	
			}
		}
	}	

	if (nr_of_acquisitions >= MIN_NR_OF_ACQUISITIONS) {
		if((get_power() > MAX_POWER) && (g_se_current_state == S_CHARGING)) {

			g_error = POWER_ERROR;
			create_event(E_ERROR);	
		}	  
	}
}


/*******************************************************************************
* Function Name  : 	determine_error_for_channel
* Description    : 	finds the matching error for a given ADC_Channel
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
e_error determine_error_for_channel(e_ADC_channel channel) {
	e_error error;
	switch (channel) {
		case ADC_U_IN:
			error = U_IN_ERROR;
			break;
		case ADC_I_IN:
			error = I_IN_ERROR;
			break;
		case ADC_U_BAT:
			error = U_BAT_ERROR;
			break;
		default:
			break;
	}
	return error;
}
