// Includes ////////////////////////////////////////////////////////////////////
#include "CPLD.h"

#include "ADC.h"
#include "CAN.h"
#include "gpio.h"
#include "SPI.h"
#include "stm32f10x.h"
#include "timer.h"
// locals //////////////////////////////////////////////////////////////////////
static signed short g_power_holdback_time = 0;		// Time until the power transmission starts	
static const double c_default_pulse_generator_times[20][2] = 
									  // switching_value	power_holdback_value
									   {{468,       		0},//{PI * sqrt(LS * C0 * 1) / 1e-9,        0},	 	// 1 Transducer
		  							    {662,				0},//{PI * sqrt(LS * C0 * 2) / 1e-9,        0},	 	// 2 Transducer
									    {811,				0},//{PI * sqrt(LS * C0 * 3) / 1e-9,        0},   	// 3 Transducer
									    {937,				0},//{PI * sqrt(LS * C0 * 4) / 1e-9,        0},		// 4 Transducer
									    {1047,				0},//{PI * sqrt(LS * C0 * 5) / 1e-9,        0},     // 5 Transducer
									    {1147,				0},//{PI * sqrt(LS * C0 * 6) / 1e-9,        0},     // 6 Transducer
									    {1239,				0},//{PI * sqrt(LS * C0 * 7) / 1e-9,        0},     // 7 Transducer
									    {1325,				0},//{PI * sqrt(LS * C0 * 8) / 1e-9,        0},     // 8 Transducer
									    {1405,				0},//{PI * sqrt(LS * C0 * 9) / 1e-9,        0},     // 9 Transducer
									    {1481,				0},//{PI * sqrt(LS * C0 * 10) / 1e-9,       0},     // 10 Transducer
									    {1554,				0},//{PI * sqrt(LS * C0 * 11) / 1e-9,       0},     // 11 Transducer
									    {1623,				0},//{PI * sqrt(LS * C0 * 12) / 1e-9,       0},     // 12 Transducer
									    {1689,				0},//{PI * sqrt(LS * C0 * 13) / 1e-9,       0},     // 13 Transducer
									    {1753,				0},//{PI * sqrt(LS * C0 * 14) / 1e-9,       0},     // 14 Transducer
									    {1814,				0},//{PI * sqrt(LS * C0 * 15) / 1e-9,       0},     // 15 Transducer
										{1874,				0},//{PI * sqrt(LS * C0 * 16) / 1e-9,       0},     // 16 Transducer
										{1931,				0},//{PI * sqrt(LS * C0 * 17) / 1e-9,       0},     // 17 Transducer
										{1987,				0},//{PI * sqrt(LS * C0 * 18) / 1e-9,       0},     // 18 Transducer
										{2042,				0},//{PI * sqrt(LS * C0 * 19) / 1e-9,       0},     // 19 Transducer
										{DEFAULT_SWITCHING_TIME_20_TX, 0}}; //TODO//{2095,				0}};//{PI * sqrt(LS * C0 * 20) / 1e-9,      0}};    // 20 Transducer
static const s_pole_type transducer_pole[2] = 
									//   fstart	fpeak  approach
									   {{50e3,  44e3,  HIGH_TO_LOW},
										{125e3, 133e3, LOW_TO_HIGH}};

										
// globals /////////////////////////////////////////////////////////////////
// Transducer 
unsigned int   g_fout_soll = DEFAULT_FOUT_SOLL;		// target frequency of the generator
unsigned int   g_Pout_soll = DEFAULT_POUT_SOLL; 		//
unsigned short g_nr_of_Txd = 20;       	// number of Transducers
unsigned short g_current_pole = 0;

// Fractional-N 
unsigned short g_integer_N = 2000;		// initialised for 25kHz
unsigned short g_fractional_N = 0;


// Pulse design 
const double c_AUM_Time  = 150;     	// time to reload the switched capacitor. Txd-voltage measurement in ns	

unsigned short g_dead_time = DEFAULT_DEAD_TIME_NS;		// dead Time in ns
unsigned short g_switching_time = 0;		// Polarity inversion time
unsigned short g_power_percentage = DEFAULT_POWER_PERCENTAGE;

unsigned short g_overlap_time = DEFAULT_OVERLAP_TIME;

unsigned short g_dead_value = 12;		// dead Time
unsigned short g_switching_value = 0;	// Preload of the inductance
unsigned short g_power_holdback_value = 0;		// When the power-transmission starts

int g_wobble_period = 5;					//Period in 10ms
int	g_wobble_max = 10;					//Amplitude of Frequenzy-Offset in per thousand
int g_t_wobble = 10;
int	g_c_wobble = 0;					  	// counter for wobble

//		Variables for the sweep mode
int g_t_sweep=500;
int g_sweep_direction = SWEEP_DOWN;
int	g_f_step=1000;
int g_f_sweep_max=200000;
int g_f_sweep_min=20000;
int g_f_sweep_start=20000;
int g_f_sweep_stop =170000;
int g_sweep_mode=0;

int	g_f_start = 200000;

int g_degas = 0;

e_selected_mode g_selected_mode = MODE_MVPT;


// local function prototypes //////////////////////////////////////////////////
void calc_low_power_mode(void);
			
			
// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	init_CPLD
* Description    : 	inits the CPLD
* Input          : 	int configuration: OLIMEX_BOARD or ULTRASONIC_GENERATOR
*			     	int CAN:           IF OLIMEX_BOARD: CAN or not CAN
*					int bandwidth      calculates the start and stop frequency for the MPP-Algorithm
*									   for example bandwidth = 0.1:
*									   If f_out_soll is 100 kHz, the algorithm starts with the frequency 110 kHz
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli, P.Haldi
* Date:          :  07. Juni 2012
*******************************************************************************/
void init_CPLD(void) {
	// Transmit version:
	CAN_send_Msg(0,0);
	insert_delay(10);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAN_send_Msg(0,0);
	insert_delay(10);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAN_send_Msg(0,0);
	insert_delay(10);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAN_send_number(86, g_version);	//send version
	
	// Initialize CPLD:
	reset_CPLD();
	GPIO_SetBits(GPIOB, NSS);       //Set NSS
	GPIO_ResetBits(GPIOB, P_BOOT);
	GPIO_ResetBits(GPIOB, S_BOOT);
	GPIO_ResetBits(GPIOC, FET_ENABLE);
	run_CPLD();
	
	//Get Address for CAN and transmit it:
	g_CAN_address =  GPIO_ReadInputDataBit(GPIOC , address0) +
					(GPIO_ReadInputDataBit(GPIOC , address1) << 1) +
					(GPIO_ReadInputDataBit(GPIOC , address2) << 2) +
					(GPIO_ReadInputDataBit(GPIOC , address3) << 3) +
					(GPIO_ReadInputDataBit(GPIOC , address4) << 4);
	CAN_send_Msg (0,0);
	insert_delay(10);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAN_send_number(99, (int)g_CAN_address);
	CAN_send_Msg (0,0);
	insert_delay(10);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAN_send_number(102, 0); //Nicht unterst�tzt//CAN_send_number(102, (int)g_ringosc_freq/10000);
	insert_delay(10);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	CAN_send_Msg (0,0);

	// generate the standard values for the fractional-N-Counter and TODO
	set_default_times(g_nr_of_Txd);
	//calc_pulse_pattern_values(); TODO: n�tig?
	calc_fractional_N_values(g_fout_soll);
	send_SPI_messages();							// Send the SPI data 
	insert_delay(2);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	//calculate sweep direction:
	calc_sweep();
	insert_delay(2);//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!vorsicht Delay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}


/*******************************************************************************
* Function Name  : 	approach_pole
* Description    : 	approaches a given pole until the defined power is achieved
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void approach_pole() {
	if (transducer_pole[g_current_pole].approach == HIGH_TO_LOW) {
		//TODO: implement
	} else {
	
	}
}


/*******************************************************************************
* Function Name  : 	reset_CPLD
* Description    : 	puts the CPLD in a defined state. All the functions are
*					deactivated
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void reset_CPLD(void) {
	#ifdef CONFIGURATION_ULTRASONIC_GENERATOR
		GPIO_ResetBits	(GPIOA, CPLD_enable_from_uC); 		//reset
	#elif defined CONFIGURATION_OLIMEX_BOARD
		GPIO_ResetBits	(GPIOB, CPLD_enable_from_uC); 		//reset
	#else
		#error "No configuration selected!"
	#endif
}


/*******************************************************************************
* Function Name  : 	run_CPLD
* Description    : 	enables the CPLD (Opposite of reset_CPLD) Yet, the FET_enable-
*					Signal has to be enabled as well, in order to operate the 
*					CPLD
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void run_CPLD(void) {
	#ifdef CONFIGURATION_ULTRASONIC_GENERATOR
		GPIO_SetBits	(GPIOA, CPLD_enable_from_uC); 		//set
	#elif defined CONFIGURATION_OLIMEX_BOARD
		GPIO_SetBits	(GPIOB, CPLD_enable_from_uC); 		//set
	#else
		#error "No configuration selected!"
	#endif
}


/*******************************************************************************
* Function Name  : 	calc_fractional_N_values
* Description    : 	calculates the values g_integer_N and g_fractional_N in order
*					to use the fractional-N-divider
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void calc_fractional_N_values(unsigned int fout) {
	double scalingfactor_exact;
	if (fout >= 20e3 && fout <= 200e3) {
		scalingfactor_exact = QUARZ_FREQUENCY / fout;
		g_integer_N = (short int)scalingfactor_exact;
		g_fractional_N = (scalingfactor_exact - g_integer_N) / 0.25;
	} else {
		g_error = FREQUENCY_ERROR;
	}
}


/*******************************************************************************
* Function Name  : 	calc_pulse_pattern_values
* Description    : 	convert g_dead_time,  g_switching_time,  g_power_holdback_time
*					to 		g_dead_value, g_switching_value, g_power_holdback_value
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
void calc_pulse_pattern_values(void) {
	g_dead_value    			= (double)g_dead_time 				* 1e-9 * QUARZ_FREQUENCY + 1;		//with increment, so it is not possible to obtain zero as result			  
	g_switching_value		= (double)g_switching_time 	* 1e-9 * QUARZ_FREQUENCY + g_dead_value + 0.5;		// round
	calc_low_power_mode();
	g_power_holdback_value		= (double)g_power_holdback_time 	* 1e-9 * QUARZ_FREQUENCY + g_switching_value + 0.5;
}


/*******************************************************************************
* Function Name  : 	calc_low_power_mode
* Description    : 	calulates the values for the low-power-mode
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void calc_low_power_mode(void) {
	double maximal_power_time;
	maximal_power_time = (double)1 / g_fout_soll / 2 - g_dead_time * 1e-9 - g_switching_time * 1e-9;
	g_power_holdback_time = maximal_power_time / 1e-9 * (100 - g_power_percentage) / 100 - g_overlap_time;
}


/*******************************************************************************
* Function Name  : 	set_default_values
* Description    : 	load the standard values
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
void set_default_times(int nr_of_Txd) {
   	g_dead_time				=	DEFAULT_DEAD_TIME_NS;					  
	g_switching_time	=	c_default_pulse_generator_times[nr_of_Txd - 1][DEFAULT_SWITCHING_TIME];
	g_power_holdback_time	=	c_default_pulse_generator_times[nr_of_Txd - 1][DEFAULT_POWER_HOLDBACK_TIME];
	g_overlap_time 			= 	DEFAULT_OVERLAP_TIME;
	
	calc_pulse_pattern_values();
}


/*******************************************************************************
* Function Name  : 	check_values
* Description    : 	checks if the pulse_pattern_values are correct 
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, P.Haldi
* Date:          :  18.2.2013
*******************************************************************************/
void check_values(void) {
	if (	g_dead_value			< 1 		|| g_dead_value				> 255 || g_dead_value > g_power_holdback_value ||
			g_switching_value 		< 1 		|| g_switching_value		> 255 || g_switching_value	< g_dead_value ||
			g_power_holdback_value	< 1 		|| g_power_holdback_value	> 2500||
			g_integer_N 			< 250		|| g_integer_N				> 2500||	//200kHz und 20kHz @50MHz 
			g_fractional_N			> 3
	
	) { 
		reset_CPLD();
	  	g_error = VALUE_ERROR;
	}
}


/*******************************************************************************
* Function Name  : 	calc_sweep 
* Description    : 	This function calculate if the sweep mode must increment or 
					decrement the frequenz
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli
* Date:          :  12.3.2013
*******************************************************************************/
void calc_sweep(void)
{
	if (g_f_sweep_stop >= g_f_sweep_start)
	{
		g_sweep_direction = SWEEP_UP;
		g_f_sweep_max =	g_f_sweep_stop;
		g_f_sweep_min =	g_f_sweep_start;
	} 
	else
	{
		g_sweep_direction = SWEEP_DOWN;
		g_f_sweep_max =	g_f_sweep_start;
		g_f_sweep_min =	g_f_sweep_stop;
	}
}

/*******************************************************************************
* Function Name  : 	MPPT (maximum power point tracker)
* Description    : 	In this funktion the MPPT is executed.
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli
* Date:          :  4.4.2013
*******************************************************************************/
double MPPT(void)
{
	static int g_f_old = 0;
	static int g_power_old = 0;
	int f_new=0;
	int f_max=(double)g_f_start*1.05;
	int f_min=(double)g_f_start*0.95;
	int step = g_fout_soll >> 11;
	double power;

	power=(double)g_ADC_value_measured[0]*(double)g_ADC_value_measured[1];


	if(g_power_old >= power)
	{
		if (g_f_old >=g_fout_soll)
		{
			f_new=g_fout_soll+step;	
		}
		else
		{
			f_new=g_fout_soll-step;
		}
	}
	else
	{
		if (g_f_old >=g_fout_soll)
		{
			f_new=g_fout_soll-step;	
		}
		else
		{
			f_new=g_fout_soll+step;
		}
	}
	// limit element		   
	if(f_new>=f_max){f_new=f_max-2*step;}//g_error=Reg_tosc_error;}
	if(f_new<=f_min){f_new=f_min+2*step;}//g_error=Reg_tosc_error;}
	g_f_old=g_fout_soll;
	g_fout_soll=f_new;
	g_power_old=power;
	return power;
}


/*******************************************************************************
* Function Name  : 	wobble
* Description    : 	This functon calculates the wobble of the frequency
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli
* Date:          :  5.4.2013
*******************************************************************************/
void wobble(void)
{
	int offset;
	g_c_wobble++;
	if(g_c_wobble >= g_wobble_period)
	{
		g_c_wobble = 0;			 
	}
	offset = g_wobble_max * g_fout_soll * g_c_wobble;
	offset = offset / (1000 * g_wobble_period);
	//PWMset(g_fin + offset, 500, 3);
}
