// Includes ////////////////////////////////////////////////////////////////////
#include "main.h"
    
#include "ADC.h"
#include "converter.h"
#include "GPIO.h"
#include "led.h"
#include "NVIC.h"
#include "RCC.h"
#include "state_machine.h"   
#include "stm32f10x.h"
#include "timer.h"
 
// globals ////////////////////////////////////////////////////////////////////
int g_version = 0;			 //software version
e_error g_error = NO_ERROR;	
int sleep_enable = 0;



// local function prototypes //////////////////////////////////////////////////

// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	main
* Description    : 	the main program
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  B.Binggeli, P.Haldi
* Date:          :  22.8.2012
*******************************************************************************/
int main(void)
{ 
	int curTimeStamp;
	volatile int i;
//	int debug_cur_power;

	/*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */ 
                                                                                               
	// Initialize peripherals:
	sleep_enable = 0;
	RCC_init();
	NVIC_init();
	GPIO_init();
	TIM_init();
	while(GPIO_ReadInputDataBit(GPIOC, KEY1));
	Systemtick_init();		
	ADC_init();
	led_init();
	init_converter();

	for (;;) {
		curTimeStamp = g_tick1ms;   // Read current Systemtick 

		// read the fast ADC inputs all 100us:
		// this function is executed in stm32f10x_it.c in the systick-interrupt
		
		ADC_Error_handler();	
		
		// don't turn on the powerconverter during Error-State	
		switch (g_se_current_state) {
// 			volatile int debug_SOC;
// 			volatile int debug_power;
 			volatile int debug_diff;
			case S_INIT:
		
			break;
			case S_DISCHARGING:
//				debug_SOC = get_SOC();
				if ((g_ADC_value_filtered[ADC_U_IN] >= u_sol_thr) && (get_SOC() < 90)) {
					create_event(E_START_MPPT);
				} else {
					if (!(curTimeStamp % (T_RESET_U_THR))) { // * 5 / 10000))) {
						u_sol_thr = U_SOL_THR_START;
					}
					if (get_SOC() == 0) {
						// TODO
					}
					set_blinking_mode(BLINKING_MODE_DISCHARGING, get_SOC());
					blink(curTimeStamp);
				}
			break;
			case S_MPPT:
				if (!(curTimeStamp % T_MPPT_STEP)) {
					mppt_step();
					set_blinking_mode(BLINKING_MODE_MPPT, 1);
				}
			break;
			case S_CHARGING_CI:
				if (!(curTimeStamp % T_CHARGING_CHECK)) {
//					debug_power = get_power() / 10000.0;
//					debug_SOC = get_SOC();
//					debug_diff = get_power() - P_MIN;
//					debug_cur_power = get_power();
					if (get_SOC() >= 100) {
						create_event(E_FULL);

					} else if (get_power() < P_MIN) {
						create_event(E_NO_SUN);
					} else {
						set_blinking_mode(BLINKING_MODE_CHARGING_CI, get_SOC());
						wobble(WOBBLE_P_MAX);
					}
				}
				if (!(curTimeStamp % T_RETRACK)) {
					create_event(E_START_MPPT);
				}
			break;
			case S_CHARGING_CU:
				if (!(curTimeStamp % T_CHARGING_CHECK)) {
					static int undervoltage_count = 0;
//					debug_power = get_power() / 10000.0;
//					debug_SOC = get_SOC();
//					debug_diff = get_power() - P_MIN;
// 					if (g_ADC_value_filtered[ADC_U_BAT] > U_BAT_FULL + 200) {
// 					//	g_error = CU_ERROR;
// 					//	create_event(E_ERROR); TODO
// 					} 
					if (g_ADC_value_filtered[ADC_I_IN] < i_charge * 3 / 100) {
						create_event(E_FULL);
					} else if (get_power() < P_MIN) {
						create_event(E_NO_SUN);
					} else if (g_ADC_value_filtered[ADC_U_BAT] < U_BAT_FULL - 200) {
						if (++undervoltage_count > 100) {
							create_event(E_NO_SUN);
						}
					} else {
						undervoltage_count = 0;
						set_blinking_mode(BLINKING_MODE_CHARGING_CU, get_SOC());
						wobble(WOBBLE_U_CONST);
					}
				}
			break;
			case S_ERROR:
				
			break;
			default:
				
			break;
		}
		if (!(curTimeStamp % T_SET_OUTPUT_CHANNELS)) {    
			setOutputChannels();
		}
		blink(curTimeStamp);
		// State Machine:
		if (!(curTimeStamp % T_SM)) {    		
			call_state_machine();
		}
		// Wait until next millisecond:
		while (curTimeStamp == g_tick1ms) {
			__WFI();
		} 
	}
}
