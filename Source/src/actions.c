// Includes ////////////////////////////////////////////////////////////////////
#include "actions.h"

#include "ADC.h"
#include "converter.h"
#include "GPIO.h"
#include "led.h"
#include "state_machine.h"
#include "stm32f10x.h"
#include "timer.h"



/*******************************************************************************
* Function Name  : 	aa_mppt_init
* Description    : 	Inits the mppt
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void aa_mppt_init(void)
{
	mppt_init();
}


/*******************************************************************************
* Function Name  : 	aa_error
* Description    : 	counts the errors and sends a message after 50 errors
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli
* Date:          :  20.6.2013
*******************************************************************************/
void aa_error(void)
{
	if (g_error == U_IN_ERROR) {
		set_blinking_mode(BLINKING_MODE_ERROR_U_IN, 100);
	}
	if (g_error == I_IN_ERROR) {
		set_blinking_mode(BLINKING_MODE_ERROR_I_IN, 100);
	}
	if (g_error == U_BAT_ERROR) {
		set_blinking_mode(BLINKING_MODE_ERROR_U_BAT, 100);
	}
	if (g_error == POWER_ERROR) {
		set_blinking_mode(BLINKING_MODE_ERROR_POWER, 100);
	}
	if (GPIO_ReadInputDataBit(GPIOC, KEY1)) {
		while (GPIO_ReadInputDataBit(GPIOC, KEY1));
		create_event(E_AKK_ERROR);
	}
}


/*******************************************************************************
* Function Name  : 	aa_clear_error
* Description    : 	resets the error-state
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli, P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void aa_clear_error(void)
{	
	g_error = NO_ERROR;

}

void aa_enter_low_power_mode(void) {
	SysTick_Config(SystemCoreClock / 10000);//50); // maximally 16777215 TODO 
	
}

void aa_enter_normal_power_mode(void) {
	SysTick_Config(SystemCoreClock / 10000);
	
}
