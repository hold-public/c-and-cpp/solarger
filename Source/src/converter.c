// Includes ////////////////////////////////////////////////////////////////////
#include "converter.h"

#include "ADC.h"
#include "gpio.h"
#include "state_machine.h"
#include "stm32f10x.h"
#include "timer.h"
// globals //////////////////////////////////////////////////////////////////////
float D_optimal;
//volatile int debug_p_vals[(int)((D_MAX - D_MIN) / MPPT_STEP ) + 5];
int u_sol_thr;// = 12000;
int i_charge;

//char first_time
// locals //////////////////////////////////////////////////////////////////////
static int p_mpp;
static float D_mppt;
static OutputChannelType OutputChannel[NR_OF_OUTPUT_CHANNELS];
static float D_max_wobble;

void init_converter(void) {
	u_sol_thr = U_SOL_THR_START;
	
	D_optimal = D_MIN;
	
	OutputChannel[0].pin_name = OUTPUT_CHANNEL_0; // Extension cable
	OutputChannel[0].SOC_min  = 10;
	OutputChannel[0].SOC_max  = 100;
	OutputChannel[0].active_level = 1;
	
	OutputChannel[1].pin_name = OUTPUT_CHANNEL_1; // Right plug
	OutputChannel[1].SOC_min  = 10;
	OutputChannel[1].SOC_max  = 100;
	OutputChannel[1].active_level = 1;
	
	OutputChannel[2].pin_name = OUTPUT_CHANNEL_2; // Power channel
	OutputChannel[2].SOC_min  = 50;
	OutputChannel[2].SOC_max  = 100;
	OutputChannel[2].active_level = 0;
	
	stop_conversion();
}
void set_duty_cycle(float D) {
	TIM3->CCR1 = TIMER_PERIOD * D / 100;
}

void start_conversion(void) {
	/*volatile int i;
	set_duty_cycle(0);
	for(i = 0; i < 100000; i++);
	__disable_irq();
	GPIO_SetBits(GPIOB, STARTING_FET);
	for(i = 0; i < 100; i++);
	GPIO_ResetBits(GPIOB, STARTING_FET);
	__enable_irq();*/
	set_duty_cycle(D_optimal);

}

void stop_conversion(void) {
	volatile int i;
	set_duty_cycle(0);
	for(i = 0; i < 10000; i++);
}

int get_power(void) {
	return g_ADC_value_filtered[ADC_U_IN] * g_ADC_value_filtered[ADC_I_IN];
}

int get_SOC(void) {
	double u_bat;
	u_bat = g_ADC_value_filtered[ADC_U_BAT];
	if (u_bat < U_BAT_EMPTY) {
		return 0;
	} else if (u_bat > U_BAT_FULL) {
		return 100;
	} else {
		return 100 * (u_bat - U_BAT_EMPTY) / (U_BAT_FULL - U_BAT_EMPTY);
	}
}

void mppt_init(void) {
	volatile int i;
	D_mppt = D_MIN;
	p_mpp = 0;
	D_optimal = D_MIN;
	set_duty_cycle(D_mppt);
	for(i = 0; i < 100000; i++);
}

void mppt_step(void) {
	int p_current;
	p_current = get_power();
// 	debug_p_vals[(int)((D_mppt - D_MIN) / MPPT_STEP)] = p_current;
	if (g_ADC_value_filtered[ADC_U_BAT] > U_BAT_FULL) {
		create_event(E_STOP_MPPT);
	} else {
		if (p_current > p_mpp) {
			p_mpp = p_current;
			D_optimal = D_mppt;
		}
		if (D_mppt + MPPT_STEP <= D_MAX) {
			D_mppt += MPPT_STEP;
			set_duty_cycle(D_mppt);
		} else {
			create_event(E_STOP_MPPT);
		}
	}
}

void save_U_IN(void) {
	u_sol_thr = g_ADC_value_filtered[ADC_U_IN] + U_SOL_THR_HYSTERESIS;
}

void save_I_IN(void) {
	i_charge = g_ADC_value_filtered[ADC_I_IN];
}

void init_CU(void) {
	D_max_wobble = D_optimal;
}

void setOutputChannels(void){
	int i;
	int SOC;
	SOC = get_SOC();
	for(i = 0; i < NR_OF_OUTPUT_CHANNELS; i++) {
		if ((SOC >= OutputChannel[i].SOC_min + SOC_HYSTERESIS) && (SOC <= OutputChannel[i].SOC_max)) {
			if(OutputChannel[i].active_level == 1) {
				GPIO_SetBits(GPIOC, OutputChannel[i].pin_name);
			} else {
				GPIO_ResetBits(GPIOC, OutputChannel[i].pin_name);
			}
		} 
		if (!((SOC >= OutputChannel[i].SOC_min) && (SOC <= OutputChannel[i].SOC_max))){
			if(OutputChannel[i].active_level == 1) {
				GPIO_ResetBits(GPIOC, OutputChannel[i].pin_name);
			} else {
				GPIO_SetBits(GPIOC, OutputChannel[i].pin_name);
			}
		}
	}
}

void wobble(e_Wobble_mode wobble_mode) {
	if (wobble_mode == WOBBLE_P_MAX) {
		more_power();
	} else if(wobble_mode == WOBBLE_U_CONST) {
		if (g_ADC_value_filtered[ADC_U_BAT] > U_BAT_FULL) {
			D_optimal -= WOBBLE_STEP;
		} else {
			D_optimal += WOBBLE_STEP;
		}	
		
		if (D_optimal < D_MIN) {
			D_optimal = D_MIN;
		}
		if (D_optimal > D_max_wobble) {
			D_optimal = D_max_wobble;
		}
		set_duty_cycle(D_optimal);
	}
}

void more_power(void) {
	static int last_power = -1;
	static e_Wobble_direction wobble_direction_more_power = WOBBLE_UP;
	static e_Wobble_direction current_wobble_direction = WOBBLE_UP;
	int current_power;
	
	current_power = get_power();
	if (current_power >= last_power) {
		wobble_direction_more_power = current_wobble_direction;
	} else {
		wobble_direction_more_power = (e_Wobble_direction)(!current_wobble_direction);
	}
	
	current_wobble_direction = wobble_direction_more_power;

	
	if (current_wobble_direction == WOBBLE_UP) {
		D_optimal += WOBBLE_STEP;
	} else {
		D_optimal -= WOBBLE_STEP;
	}
	if (D_optimal < D_MIN) {
		D_optimal = D_MIN;
	}
	if (D_optimal > D_MAX) {
		D_optimal = D_MAX;
	}
	set_duty_cycle(D_optimal);
	last_power = current_power;
}
	
