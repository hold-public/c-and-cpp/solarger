// Includes ////////////////////////////////////////////////////////////////////
#include "GPIO.h"

#include "main.h"
#include "stm32f10x.h"


// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	GPIO_init
* Description    : 	initializes the Input/Output-Pins, dependant of the chosen 
*					configuration (ULTRASONIC_GENERATOR or OLIMEX_BOARD)
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli, P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void GPIO_init(void)
{
  	GPIO_InitTypeDef GPIO_InitStructure;

/****************************************************************************************************/
/******************Digital Outputs ******************************************************************/
	// Port A:
	
	// Port B:
	GPIO_InitStructure.GPIO_Pin = LED1 | LED2;// | STARTING_FET;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	
	
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_NoJTRST, ENABLE);		// in order to use PB4
		
	// Port C:
	GPIO_InitStructure.GPIO_Pin = OUTPUT_CHANNEL_0 | OUTPUT_CHANNEL_1 | OUTPUT_CHANNEL_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	

/****************************************************************************************************/
/******************Digital Inputs *******************************************************************/
	// Port A:
		
	// Port B:
/*	GPIO_InitStructure.GPIO_Pin = STARTING_FET_CONTROL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;		// Floating-Input
	GPIO_Init(GPIOB, &GPIO_InitStructure);*/
	// Port C:
	GPIO_InitStructure.GPIO_Pin = KEY1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;		// Floating-Input
	GPIO_Init(GPIOC, &GPIO_InitStructure);


/****************************************************************************************************/
/*******************Analog inputs (Sensors)**********************************************************/

	// Port A:
	GPIO_InitStructure.GPIO_Pin = U_U_in | U_I_in | U_U_bat;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;		// Analog-Eingang
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// Port B:

	// Port C:
	
/****************************************************************************************************/
/*******************TIMER****************************************************************************/	
	GPIO_InitStructure.GPIO_Pin = uC_to_FET;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
} // end init_GPIO


