// Includes ////////////////////////////////////////////////////////////////////
#include "led.h"

#include "ADC.h"
#include "gpio.h"
#include "stm32f10x.h"
#include "timer.h"
// locals //////////////////////////////////////////////////////////////////////
static int blinking_period;
static int LED1_T_on, LED1_T_off, LED2_T_on, LED2_T_off;
// globals //////////////////////////////////////////////////////////////////////

void led_init(void) {
	set_blinking_period(DEFAULT_BLINKING_PERIOD);
	set_blinking_mode(BLINKING_MODE_DISCHARGING, 0);
	GPIO_ResetBits(GPIOB, LED1);
	GPIO_ResetBits(GPIOB, LED2);
}

void blink(int curTimeStamp) {
	int curBlinkingTime = curTimeStamp % (blinking_period + 1);
	if (curBlinkingTime == LED1_T_on) {
		GPIO_SetBits(GPIOB, LED1);
	}
	if (curBlinkingTime == LED1_T_off) {
		GPIO_ResetBits(GPIOB, LED1);
	}
	if (curBlinkingTime == LED2_T_on) {
		GPIO_SetBits(GPIOB, LED2);
	}
	if (curBlinkingTime == LED2_T_off) {
		GPIO_ResetBits(GPIOB, LED2);
	}
}							

void set_blinking_period(int blinking_period_set) {
	blinking_period = blinking_period_set;
}
void set_blinking_times(int LED1_pz_on, int LED1_pz_off, int LED2_pz_on, int LED2_pz_off) {
	LED1_T_on  = (blinking_period * LED1_pz_on)  / 100;
	LED1_T_off = (blinking_period * LED1_pz_off) / 100;
	LED2_T_on  = (blinking_period * LED2_pz_on)  / 100;
	LED2_T_off = (blinking_period * LED2_pz_off) / 100;
}

void set_blinking_mode(e_blinking_mode_type blinking_mode, int q) {
	switch (blinking_mode) {
		case BLINKING_MODE_DISCHARGING:
			set_blinking_times(0, 0, 0, 0);
			break;
		case BLINKING_MODE_MPPT:
			set_blinking_times(0, 50, 25, 75);
			break;
		case BLINKING_MODE_CHARGING_CI:
			set_blinking_times(0, q, 0, 0);
		break;
		case BLINKING_MODE_CHARGING_CU:
			set_blinking_times(0, 0, 0, q);
		break;
		case BLINKING_MODE_ERROR_U_IN:
			set_blinking_times(0, 5, 0, 5);
		break;
		case BLINKING_MODE_ERROR_I_IN:
			set_blinking_times(0, 5, 0, 95);
		break;
		case BLINKING_MODE_ERROR_U_BAT:
			set_blinking_times(0, 95, 0, 5);
		break;
		case BLINKING_MODE_ERROR_POWER:
			set_blinking_times(0, 95, 0, 95);
		break;

		default:
			break;
	}
}
