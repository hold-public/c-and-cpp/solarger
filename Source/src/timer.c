// Includes ////////////////////////////////////////////////////////////////////
#include "timer.h"

#include "GPIO.h"
#include "stm32f10x.h"


// globals ////////////////////////////////////////////////////////////////////
//Sys tick timer
volatile int g_tick1ms = 1;		     //for 1ms time stamp
volatile int g_tick1ms_resetable = 0;	 //used for a resetable counter
volatile int g_tick100us = 1;		 //for 100us time stamp


// functions //////////////////////////////////////////////////////////////////
/*******************************************************************************
* Function Name  : 	Systemtick_init
* Description    : 	Configures the System Tick
*
*					clock is 72MHz 
* 					SysTick_Config configures the System Tick. The uint32_t - value is the count-value 
* 					If the System Tick reached this value it generates an interrupt, which can be handled
* 					with the function systick_Handler()
*
* 					so to count for example from 0 to 7200 means an interrupt period of 100us
* Input          : 	None
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth, B.Binggeli, P.Haldi
* Date:          :  20.6.2013
*******************************************************************************/
void Systemtick_init(void)
{
	SysTick_Config(SystemCoreClock / 10000);
} 

/*******************************************************************************
* Function Name  : TIM_init
* Description    : Configures TIM3
* Input          : None
* Output         : None
* Return         : None	 
* 
* clock is 72MHz 
* the timer has a prescale of 1
* 
* the resolution of T is 13.8889 ns
* 
* fout =1/(n*13.8889 e-9)
*******************************************************************************/
void TIM_init(void)
{ 
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
 

/****TIM3 configuration for fin *******************************************************/
/* Is configured for a PWM-Signal */
	
	// Timer 3 initialisation ///////////////////////////////////////////
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_Period = TIMER_PERIOD; //1800
	TIM_TimeBaseStructure.TIM_Prescaler = 0;	// no prescaling

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
	TIM_InternalClockConfig(TIM3);

	// OC initialisation ///////////////////////////////////////////	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = 0;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);	

	TIM_ARRPreloadConfig(TIM3, ENABLE);	
	//TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Repetitive);
	//TIM_SelectOCxM 	(TIM3, TIM_Channel_1, TIM_OCMode_PWM1);
	


	TIM_Cmd(TIM3, ENABLE);
}


/*******************************************************************************
* Function Name  : 	insert_delay
* Description    : 	pauses the controller for a maximum of the given time
* Input          : 	number of miliseconds
* Output         : 	None
* Return         : 	None
* Author:        :  S.Weyeneth
* Date:          :  07. Juni 2012
*******************************************************************************/
void insert_delay (int ms) {
  g_tick1ms_resetable = 0;
  while (g_tick1ms_resetable < ms){}																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																					
																																																																																																																																																																																																																																																																																																																																																																																																																						
}
