// #include "stm32f10x.h"
// #include "uart.h"
// #include "globals.h"


// /* Private variables ---------------------------------------------------------*/
// USART_InitTypeDef USART_InitStructure;


// void UART_init(void)
// {
// /*USART2 configuration ------------------------------------------------------*/
//   /* USART2 configured as follow:
//         - BaudRate = 4800 baud  
//         - Word Length = 8 Bits
//         - One Stop Bit
//         - Even parity
//         - Hardware flow control disabled (RTS and CTS signals)
//         - Receive and transmit enabled
//   */
//   USART_InitStructure.USART_BaudRate = 4800;
//   USART_InitStructure.USART_WordLength = USART_WordLength_8b;
//   USART_InitStructure.USART_StopBits = USART_StopBits_1;
//   USART_InitStructure.USART_Parity = USART_Parity_No;
//   USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//   USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
//   
//   /* Configure USART2 */
//   USART_Init(USART2, &USART_InitStructure);
//    
//    /* Enable USART2 Receive and Transmit interrupts */
//   USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

//   
//   /* Enable the USART2 */
//   USART_Cmd(USART2, ENABLE);

// }

