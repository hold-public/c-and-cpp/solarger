#ifndef GPIO_H
#define GPIO_H
#include "main.h"

// Port A: //////////////////////////////////////////////
#define U_U_in					GPIO_Pin_0
#define U_I_in					GPIO_Pin_1
#define U_U_bat					GPIO_Pin_2

#define uC_to_FET				GPIO_Pin_6


// Port B: ///////////////////////////////////////////////
//#define STARTING_FET			GPIO_Pin_0
//#define STARTING_FET_CONTROL	GPIO_Pin_1
#define LED1					GPIO_Pin_4	
#define LED2					GPIO_Pin_5

// Port C: ///////////////////////////////////////////////
#define OUTPUT_CHANNEL_0		GPIO_Pin_1
#define OUTPUT_CHANNEL_1		GPIO_Pin_2
#define OUTPUT_CHANNEL_2		GPIO_Pin_3
#define KEY1					GPIO_Pin_8

/***************************************************************/
//  function prototype
extern void GPIO_init(void);


#endif 

