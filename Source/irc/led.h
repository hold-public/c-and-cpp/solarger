#ifndef LED_H
#define LED_H

// Defines //////////////////////////////////////////////////////////
#define DEFAULT_BLINKING_PERIOD  500

typedef enum {
	BLINKING_MODE_DISCHARGING,
	BLINKING_MODE_MPPT,
	BLINKING_MODE_CHARGING_CI,
	BLINKING_MODE_CHARGING_CU,
	BLINKING_MODE_ERROR_U_IN,
	BLINKING_MODE_ERROR_I_IN,
	BLINKING_MODE_ERROR_U_BAT,
	BLINKING_MODE_ERROR_POWER,
} e_blinking_mode_type;

extern void led_init(void);
extern void blink(int curTimeStamp);
extern void set_blinking_period(int blinking_period_set);
extern void set_blinking_times(int LED1_pz_on, int LED1_pz_off, int LED2_pz_on, int LED2_pz_off);
extern void set_blinking_mode(e_blinking_mode_type blinking_mode, int q);

#endif /*LED_H*/

