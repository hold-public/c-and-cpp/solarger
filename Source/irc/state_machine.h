#ifndef state_machine_H
#define state_machine_H

// includes:
//#include "state_event_table.h"


//define constants
//----------------
//the constant SE_TABLE_LENGHT gives the current length of the state event table
#define SE_TABLE_LENGTH		20//37 //

//the constant SE_NO_LINE_FOUND gives the return value of the function find_se_table_line if there was no line found in the state event table that fits to the current state and to the occured event
#define SE_NO_LINE_FOUND	0xFFFF	 
#define EVENT_STORE_SIZE	10

//define events
//-------------
//the following constants define the event that can occure in the state event machine		
typedef enum {
	E_NO_EVENT	= 99,	
	E_START,	
	E_NO_SUN,
	E_FULL,	
	E_ERROR,
	E_AKK_ERROR,
	E_START_MPPT,
	E_STOP_MPPT,
} e_event_type;


//define states
//-------------
//the following constants define the states of the state event machine
//the constants are use in files: 	state_event_table.h
typedef enum {
	S_INIT,
	S_DISCHARGING,
 	S_CHARGING_CI,
	S_CHARGING_CU,
	S_MPPT,
 	S_ERROR,
} e_state_type;


//define actions
//--------------
//the following constants define the actions the state event machine can execute
//the constants are use in files: 	state_event_table.h
//									subroutines_A.c in subroutine execute_se_action									
typedef enum {
 	A_NO_ACTION = 88,					//no action to be executed
	A_START_CONVERSION,
	A_STOP_CONVERSION,
	A_SAVE_U_IN,
	A_SAVE_I_IN,
	A_LOW_POWER,
	A_NORMAL_POWER,
	A_INIT_CU,
	A_INIT_MPPT,
	A_ERROR,
	A_CLEAR_ERROR
} e_action_type;


//define structure for state event table
//--------------------------------------
struct se_table_line 			//define a line of the state event table
{
	e_event_type event;			//the event that occured
	e_state_type current_state;		//the current state of the machine
	e_state_type new_state;		//the state to switch to
	e_action_type action[4];		//the numbers of the actions to be executed at a transition
};

//The variable g_se_current_state contains the number of the current state of the state event machine
extern e_state_type g_se_current_state;		//this variable contains the number of the current state

extern const struct se_table_line se_table[SE_TABLE_LENGTH];


//function prototypes
//-------------------
extern void call_state_machine(void);
extern void create_event(e_event_type event_number);

#endif
