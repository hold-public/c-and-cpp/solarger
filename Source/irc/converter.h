#ifndef CONVERTER_H
#define CONVERTER_H

#include "main.h"

// Defines //////////////////////////////////////////////////////////
#define D_MIN			40.0				// in %
#define D_MAX			95.0				// in %
#define MPPT_STEP  		0.1					// in %
#define WOBBLE_STEP		0.2				// in %

#define T_MPPT_STEP		10 					// in 1ms
#define T_CHARGING_CHECK 10					// in 1ms
#define T_RETRACK		(10 * 60 * 1000)  	// in 1ms: all 10 minutes
#define T_RESET_U_THR	(5 * 60 * 1000)		// in 1ms: all 5 minutes TODO: changed from 10 seconds
#define T_SET_OUTPUT_CHANNELS 10
#define P_MIN 			(0.500 * 1000 * 10000)	
#define U_BAT_EMPTY		(11.5 * 1000) // = 11.500
#define U_BAT_FULL		(14.7 * 1000)// = 9000
#define MAX_POWER 		(30 * 10000 * 1000) //maximum power, factor because I [mA] and U [V/100]
#define U_SOL_THR_START 17000
#define U_SOL_THR_HYSTERESIS 200
#define NR_OF_OUTPUT_CHANNELS	3
#define R_CONNECTION	0.317
#define SOC_HYSTERESIS 	10

typedef struct {
	int	pin_name;
	int SOC_min;
	int	SOC_max;
	int active_level;
} OutputChannelType;

typedef enum {
	WOBBLE_P_MAX,
	WOBBLE_U_CONST,
} e_Wobble_mode;

typedef enum {
	WOBBLE_DOWN = 0,
	WOBBLE_UP = 1,
} e_Wobble_direction;

typedef enum {
	POWER_UP,
	POWER_DOWN,
} e_power_direction;

void init_converter(void);
extern void set_duty_cycle(float D);
extern void start_conversion(void);
extern void stop_conversion(void);
extern int get_power(void);
extern int get_SOC(void);
extern void mppt_init(void);
extern void mppt_step(void);
extern void save_U_IN(void);
extern void save_I_IN(void);
extern void init_CU(void);
extern void setOutputChannels(void);
extern void wobble(e_Wobble_mode wobble_mode);
extern void more_power(void);

extern float D_optimal;
extern int u_sol_thr;
extern int i_charge;

#endif /*CONVERTER_H*/

