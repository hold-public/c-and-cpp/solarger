#ifndef SPI_H
#define SPI_H


//  function prototype 
extern void SPI_init(void);
extern void send_SPI_messages(void);
extern void send_2Byte_SPI(unsigned short data);
extern int parity(int);
extern void get_ringosc_frequency(void);         //frequency of the intern ringoscillator from the CPLD

#endif /*SPI_H*/
 
//*****************************************************************************

