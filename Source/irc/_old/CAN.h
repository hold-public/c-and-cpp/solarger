#ifndef CAN_H
#define CAN_H

extern char			g_CAN_address;

//  function prototype
extern void CAN_init(void); 
extern void CAN_receive_Msg(void);
extern void CAN_send_Msg (int id, int data);
extern void CAN_debug(unsigned int debug_data);
extern void monitoring (void);
extern void CAN_send_number(int id, int Nr);
#endif /*CAN_H*/

