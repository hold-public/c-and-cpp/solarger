#ifndef CPLD_H
#define CPLD_H

#include "main.h"

// Defines //////////////////////////////////////////////////////////
#define DEFAULT_POUT_SOLL				500
#define MAX_POWER 			(550 * 1000 * 100) //maximum power, factor because I [mA] and U [V/100]
#ifdef CONFIGURATION_OLIMEX_BOARD
	#define QUARZ_FREQUENCY		12e6
#elif defined CONFIGURATION_ULTRASONIC_GENERATOR
	#define QUARZ_FREQUENCY		50e6
#else
	#error "No configuration selected!"
#endif


#define TIME_BOOT_MS					500
#define TIME_FLT_CLR					1

//#define R1_C1
#define R5_C4

//#define FREQ_20
#define FREQ_100
//#define FREQ_200
#ifdef R1_C1
	#ifdef FREQ_20
		#define DEFAULT_POWER_PERCENTAGE		100
		#define DEFAULT_SWITCHING_TIME_20_TX 	750
		#define DEFAULT_DEAD_TIME_NS			1000		// in ns
		#define DEFAULT_OVERLAP_TIME			0
		#define DEFAULT_FOUT_SOLL				20000
	#elif defined FREQ_100
		#define DEFAULT_POWER_PERCENTAGE		100
		#define DEFAULT_SWITCHING_TIME_20_TX 	750
		#define DEFAULT_DEAD_TIME_NS			1100		// in ns
		#define DEFAULT_OVERLAP_TIME			0
		#define DEFAULT_FOUT_SOLL				100000
	#elif defined FREQ_200
		#define DEFAULT_POWER_PERCENTAGE		100
		#define DEFAULT_SWITCHING_TIME_20_TX 	750
		#define DEFAULT_DEAD_TIME_NS			1100		// in ns
		#define DEFAULT_OVERLAP_TIME			0
		#define DEFAULT_FOUT_SOLL				200000
	#else 
		#error "Keine Frequenz definiert!"
	#endif
#elif defined R5_C4
	#ifdef FREQ_20
// 		#define DEFAULT_POWER_PERCENTAGE		100
// 		#define DEFAULT_SWITCHING_TIME_20_TX 	500
// 		#define DEFAULT_DEAD_TIME_NS			1000		// in ns
// 		#define DEFAULT_OVERLAP_TIME			0
// 		#define DEFAULT_FOUT_SOLL				20000
	#elif defined FREQ_100
		#define DEFAULT_POWER_PERCENTAGE		100
		#define DEFAULT_SWITCHING_TIME_20_TX 	1550
		#define DEFAULT_DEAD_TIME_NS			900		// in ns
		#define DEFAULT_OVERLAP_TIME			0
		#define DEFAULT_FOUT_SOLL				100000
	#elif defined FREQ_x
	#elif defined FREQ_200
// 		#define DEFAULT_POWER_PERCENTAGE		100
// 		#define DEFAULT_SWITCHING_TIME_20_TX 	750
// 		#define DEFAULT_DEAD_TIME_NS			1100		// in ns
// 		#define DEFAULT_OVERLAP_TIME			0
// 		#define DEFAULT_FOUT_SOLL				200000
	#else 
		#error "Keine Frequenz definiert!"
	#endif
#else
	#error "Keine Last definiert!"
#endif


#define SWEEP_UP				1
#define SWEEP_DOWN				0
//#define UPPER_LEVEL_MODE  1
//#define LOWER_LEVEL_MODE  2



typedef enum {
	DEFAULT_SWITCHING_TIME	= 0,
	DEFAULT_POWER_HOLDBACK_TIME = 1
} e_DEFAULT_times;

typedef enum {
	MODE_MVPT		= 0,
	MODE_NORMAL		= 1,
	MODE_TRIANGULAR	= 3
} e_selected_mode;

typedef enum {
	HIGH_TO_LOW		= 0,
	LOW_TO_HIGH		= 1
} e_approach;

typedef struct {
	unsigned int start_frequency;
	unsigned int resonance_frequency;
	e_approach	approach;
} s_pole_type;

// Globale Variablen /////////////////////////////////////////////////
extern unsigned int   g_fout_soll;			  // target frequency of the generator
extern unsigned int   g_Pout_soll;
extern unsigned short g_nr_of_Txd;       	// number of Transducers
// Fractional-N
extern unsigned short g_integer_N;
extern unsigned short g_fractional_N;

// Pulse pattern
extern unsigned short g_dead_time;		// dead Time
extern unsigned short g_switching_time;	// Polarity inversion time
extern unsigned short g_power_percentage;
extern unsigned short g_overlap_time;

extern unsigned short g_dead_value; 	// dead Time
extern unsigned short g_switching_value;	// Polarity inversion
extern unsigned short g_power_holdback_value;		// When the power-transmission starts

extern int g_wobble_period;
extern int g_wobble_max;
extern int g_t_wobble;
extern int g_c_wobble;

//		Variables for the sweep mode
extern int g_t_sweep;
extern int g_sweep_direction;
extern int g_f_step;
extern int g_f_sweep_max;
extern int g_f_sweep_min;
extern int g_f_sweep_start;
extern int g_f_sweep_stop;
extern int g_sweep_mode;

extern int g_f_start;

extern int g_degas;

extern e_selected_mode g_selected_mode;

// Funktionen ////////////////////////////////////////////////////////
extern void init_CPLD(void);
extern void reset_CPLD(void);
extern void run_CPLD(void);
extern void calc_pulse_pattern_values(void);
extern void calc_fractional_N_values(unsigned int fout);
extern void check_values(void);
extern void set_default_times(int nr_of_Txd);
extern void calc_sweep (void);


#endif /*CPLD_H*/

