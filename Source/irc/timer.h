#ifndef TIM_H
#define TIM_H

#define TIMER_PERIOD	((uint16_t) (SystemCoreClock / 40000 - 1))

//Sys tick timer
extern volatile int g_tick1ms;		    //for time stamp
extern volatile int g_tick1ms_resetable;	//used for a resetable counter
extern volatile int g_tick100us;		 	//for 100us time stamp

//  function prototype
extern void TIM_init(void); 
extern void Systemtick_init(void);
extern void PWMset(int frequency,double duty_cycle,char Timer);
extern void Puls(int ton);
extern void insert_delay (int ms);

#endif

 
