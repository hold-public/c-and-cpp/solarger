/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef actions_H
#define actions_H




//define function prototypes
//--------------------------
extern void aa_output_stage_on(void);
extern void aa_output_stage_off(void);
extern void aa_start_mppt(void);
extern void aa_mppt(void);
extern void aa_mppt_init(void);
extern void aa_error(void);
extern void aa_clear_error(void);
extern void aa_enter_low_power_mode(void);
extern void aa_enter_normal_power_mode(void);
#endif

