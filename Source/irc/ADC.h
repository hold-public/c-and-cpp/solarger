#ifndef ADC_H
#define ADC_H

#define ADC1_DR_Address    ((u32)0x4001244C)	///< Address of ADC1 Data register (DR)

#define ADC_MAX_VALUE				4095				///< Highest measurable value. 12Bit ADC --> 4095
#define ADC_MIN_VALUE				0					///< Lowest measurable value.

#define NR_OF_CHANNELS 				3

#define SIZE_OF_PREFILTER			15

#define LIMIT_MIN					0
#define LIMIT_MAX					1

#define ADC_GAIN_U_IN				8.86230			// U_IN / 1mV   = number * U_ADC * (R1 + R2) * (1000 * 1mV / 1V) / (2^N * R2) = number * 3.3V * (100kOhm + 10kOhm) * 1000 / (2^12 * 10kOhm)
#define ADC_GAIN_I_IN				4.35059			// I_IN / 100uA = number * U_ADC * R1 * (10000 * 100uA / 1A) / (2^N * R2 * Rshunt) = number * 3.3V * 2.7kOhm * 10000 / (2^12 * 100kOhm * 50mOhm)
#define ADC_GAIN_U_BAT				5.31738			// U_BAT / 1mV  = number * U_ADC * (R1 + R2) * (1000 * 1mV / 1V) / (2^N * R2) = number * 3.3V * (56kOhm + 10kOhm) * 1000 / (2^12 * 10kOhm)

#define ADC_U_IN_MIN				0
#define	ADC_I_IN_MIN				366.0
#define ADC_U_BAT_MIN				0

#define MIN_NR_OF_ACQUISITIONS 	(Nr_of_Filtervalues[channel_nr] * 20)

extern volatile double g_ADC_value_filtered[NR_OF_CHANNELS];				// filtered values



typedef enum {
	ADC_U_IN = 0,
	ADC_I_IN = 1,
	ADC_U_BAT = 2,
} e_ADC_channel;


//  function prototype
void ADC_init(void); 
void convert_ADC_value(void);
void ADC_Error_handler(void);


#endif /*ADC_H*/


