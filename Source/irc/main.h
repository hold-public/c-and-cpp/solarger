/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef main_H
#define main_H

#define TIME_ADC_IN_100US		5
#define TIME_LED_BLINK			200
#define T_SM 					20
#define MAX_TICK_1_MS			3600000

// OLIMEX_BOARD or ULTRASONIC_GENERATOR:
//#define CONFIGURATION_OLIMEX_BOARD
#define CONFIGURATION_ULTRASONIC_GENERATOR

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for the error handling																						*/
/*------------------------------------------------------------------------------------------------------------------------------*/
typedef enum {
	NO_ERROR				= 0,
	U_IN_ERROR				= 1,
	I_IN_ERROR	    		= 2,
	U_BAT_ERROR	    		= 3,
	POWER_ERROR				= 4,
	CU_ERROR				= 5,
	ACTION_ERROR,
} e_error;


extern e_error	g_error;
extern int g_version;			 //software version
extern int sleep_enable;


#endif
