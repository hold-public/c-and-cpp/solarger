#ifndef globals_H
#define globals_H

#define up		1
#define down	0

#define on		1
#define off		0


/*==============================================================================================================================*/
/*		define global variables																									*/
/*==============================================================================================================================*/

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables used for the state event machine																				*/
/*------------------------------------------------------------------------------------------------------------------------------*/



//The variable g_se_event_occured contains the number of the event that is currently handled by the state event machine
//extern unsigned char g_se_event_occured;	//this variable contains the number of the event occured

//The array g_event_store contains the numbers of the last 10 events occured
//extern e_event_type g_event_store[10];	


//start up and shut down counter
//extern int g_counter;
//extern int g_counter_stage;

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables used for timer applications																					*/
/*------------------------------------------------------------------------------------------------------------------------------*/


extern int g_version;			//software version

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for Analog-Digtal-Converter																					*/
/*------------------------------------------------------------------------------------------------------------------------------*/

//extern volatile short g_ADCConvertedValue[8]; 	// the convertet value of the analog input
//extern volatile double g_ADC_value_filtered[NR_OF_SLOW_CHANNELS + NR_OF_FAST_CHANNELS];				// filtered values 
//extern volatile double g_sensor_filtered[NR_OF_FAST_CHANNELS];	// filtered value of Iin Usupply Usupply/2Uum2 after peak Detektion


//slow values
//extern double g_values[8]; 				// physical values of ADCConvertedValue
//extern double g_filtered_values[8][30]; 	// Array for filter the slow ADC
//extern int Nr_of_Filtervalues[] ;  		// Define here each number of Filtervalues!

//fast values
//extern int g_values_fast[5]; 		//values of ADCConvertedValue (not physical)
//extern int g_filtered_values_fast[5][4]; 	//Array for filter the fast ADC 
//extern int g_sensor_fast[5];		//filtered values
//extern int g_sensor_temp[5][21];	//last 20 ADC values
//extern int g_peak_Counter;			//Counter for peak-valuedetection

//monitoring
//extern int counter;



/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for the 3-point Converter																						*/
/*------------------------------------------------------------------------------------------------------------------------------*/
extern int g_t_c3p;





extern double g_integrator_synch_Usupply;	//Integrator for symmetrize the 3-point converter
extern int g_synch_value_old;					
extern int g_Uratio_diff_old;
extern int g_synch_value_current;

extern double g_Uumpeak_old;			//Storage for the last Peakvalue of the Transducervoltage
extern short	g_Osc_old;					//Storage for the last oscillating time value
extern short 	g_Osc_value_start;			//Start value of oscillating time 


extern unsigned int   g_Pout_soll; //
extern unsigned short g_nb_of_Txd;       // number of Transducers


extern int g_degas;



extern int	g_f_start;
extern int g_f_old;
extern int g_power_old;

/*measure optimum operation point*/ 
//extern double g_max_power;
//extern int g_f_resonanz;
//extern int g_measure_delay;

/*Variabel for power regulation*/
extern double g_power[10];
extern int g_c3p_counter;

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for the Phase shift Converter (PSC)																			*/
/*------------------------------------------------------------------------------------------------------------------------------*/
extern unsigned int g_t_supply;		 		//periode for execute PSC-regulation
extern double g_Dsupply;	 				//Duty cycle for PSC
extern double g_Usupply_soll;				//set value of the peack Voltage after the PSC-Converter
extern double	g_integrator_supply;		//Integrator PSC
extern unsigned int g_fsupply;				//frequency of PSC

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for the CPLD																									*/
/*------------------------------------------------------------------------------------------------------------------------------*/

extern volatile unsigned long int	g_fin;	       // Reference frequency for the PLL

extern volatile unsigned int	g_fout;	   // output frequency of the generator
extern double g_ringosc_freq;	   //frequency of the intern ringoscillator from the CPLD
extern double g_T_ringosc;        //Period_time of the Ringoscillator in s



/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for the SPI																									*/
/*------------------------------------------------------------------------------------------------------------------------------*/
// default values are only in case, when CAN is activated on the OLIMEX-Board

extern char		    g_M;			// Divider_value for the PLL
extern char		    g_N;	       	// Divider_value for the PLL
extern char    	   	g_div_ring;		// Divider_value for Ringoscillator

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Variables for the CAN																									*/
/*------------------------------------------------------------------------------------------------------------------------------*/


 


/*------------------------------------------------------------------------------------------------------------------------------*/
/*		tabels																													*/
/*------------------------------------------------------------------------------------------------------------------------------*/

//extern const double c_max_power;
//extern const double c_sensor_limits[8][2];                                                

/*------------------------------------------------------------------------------------------------------------------------------*/
/*		Other variables																											*/
/*------------------------------------------------------------------------------------------------------------------------------*/

extern int test1;
extern int test2;
extern double test3;
extern int test4;


#endif /*globals_H*/

